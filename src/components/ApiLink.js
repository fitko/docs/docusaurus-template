import React from "react"
import Link from '@docusaurus/Link'

export default ({to, withMethod = 'get', children}) => {
  const url = `/docs/spec#${withMethod.toLowerCase()}-${to.replace(/[{}]/g, '-')}`
  if (children) {
    return <Link to={url}>{children}</Link>
  } else {
    return <Link to={url}><code>{withMethod.toUpperCase()} {to}</code></Link>
  }
}
