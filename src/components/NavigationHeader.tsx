import React, { useState } from 'react';
import { IconMenu2, IconX, IconConfetti } from '@tabler/icons-react'
import useTranslation from '../shared/use-translation'
// @ts-ignore
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';

const { translate } = useTranslation()

// Home icon for NavBar
const IconHome = (props) => (
    <svg
        xmlns='http://www.w3.org/2000/svg'
        height='1em'
        viewBox='0 0 576 512'
        aria-hidden='true'
        {...props}
    >
        <path d='M575.8 255.5c0 18-15 32.1-32 32.1h-32l.7 160.2c0 2.7-.2 5.4-.5 8.1V472c0 22.1-17.9 40-40 40h-16c-1.1 0-2.2 0-3.3-.1-1.4.1-2.8.1-4.2.1H392c-22.1 0-40-17.9-40-40v-88c0-17.7-14.3-32-32-32h-64c-17.7 0-32 14.3-32 32v88c0 22.1-17.9 40-40 40h-55.9c-1.5 0-3-.1-4.5-.2-1.2.1-2.4.2-3.6.2h-16c-22.1 0-40-17.9-40-40V360c0-.9 0-1.9.1-2.8v-69.6H32c-18 0-32-14-32-32.1 0-9 3-17 10-24L266.4 8c7-7 15-8 22-8s15 2 21 7l255.4 224.5c8 7 12 15 11 24z' />
    </svg>
)

export default () => {
    const { siteConfig } = useDocusaurusContext();
    const { feedbackUrl, sourceCodeUrl } = siteConfig.customFields
    
    const [theMenuIsOpen, setTheMenuIsOpen] = useState(false)

    const fitkoDomain = 'https://docs.fitko.de'
    const routeHome = fitkoDomain + '/'

    return (
        <div className='portal-header'>
            {/* Ensure Tailwind classes are loaded */}
            <div className='sm:block xl:block hidden xl:hidden sm:hidden'></div>
            {/* Navbar for small screens */}
            <div className={`sm:hidden pb-2`}>
                <div className='bg-gray-100 text-black p-2 shadow shadow-gray-500/50 flex flex-row justify-end'>
                    {!theMenuIsOpen && <button type='button' title='Menü anzeigen' tabIndex={0} id='req-open' onClick={() => setTheMenuIsOpen(true)}>
                        <IconMenu2 aria-hidden='true' className='cursor-pointer'/>
                    </button>}
                    {theMenuIsOpen && <button type='button' title='Menüanzeige schließen' tabIndex={0} id='req-close' className='cursor-pointer' onClick={() => setTheMenuIsOpen(false)}>
                        <IconX aria-hidden='true' />
                    </button>}
                </div>
            </div>

            {/* Menu for small screens */}
            {theMenuIsOpen && <nav id='theMenu' className='absolute top-11 z-20 bg-white flex flex-col w-screen h-screen shadow-gray-500/50 duration-500 ease-out'>
                <ul className='text-lg ml-16'>
                    {/* home */}
                    <li className='my-4'>
                        <a href={`${routeHome}`} className='hover:underline' title='Home' onClick={() => setTheMenuIsOpen(false)}>
                            <span className='inline-flex'>
                                <span className='pr-2'>
                                    <IconHome className='h-5 pt-1'/>
                                </span>
                                { translate('navbar.labels.back')}
                            </span>
                        </a>
                    </li>
                    <li className='my-4'>
                        <a href={sourceCodeUrl} className='hover:underline' onClick={() => setTheMenuIsOpen(false)}>
                            { translate('navbar.labels.sourcecode')}
                        </a>
                    </li>
                    <li className='my-4'>
                        <a href={feedbackUrl} className='hover:underline' onClick={() => setTheMenuIsOpen(false)}>
                            { translate('navbar.labels.feedback')}
                        </a>
                    </li>
                </ul>
            </nav>}

            {/* navbar for large screens */}
            <div className={`hidden sm:block pb-2 sm:pb-4`}>
                <div className='absolute top-0 left-0 right-0 z-99 tracking-wide'>
                    <nav className='bg-gray-100 text-gray-700 px-8 gap-8 shadow shadow-gray-500/50 flex flex-row justify-between text-xs underline-offset-2 uppercase '>
                        <div className='flex gap-4 items-center'>
                            {/* home */}
                            <a href={`${routeHome}`} className='hover:underline p-1 flex' title='Home' >
                                <IconHome className='h-3.5 pt-0.5' aria-hidden='true'/>
                            </a>
                            {/* back */}
                            <a href={`${routeHome}`} className='h-3.5 hover:underline px-2'>
                                { translate('navbar.labels.back')}
                            </a>
                        </div>
                        <div className='flex gap-4 items-center'>
                            <a href={sourceCodeUrl} target='_blank' rel='noopener noreferrer' className='h-3.5 hover:underline'>
                                { translate('navbar.labels.sourcecode')}
                            </a>
                            <a href={feedbackUrl} className='h-3.5 hover:underline flex items-center'>
                                <IconConfetti className='h-3.5 mr-1' aria-hidden='true'/>
                                { translate('navbar.labels.feedback')}
                            </a>
                        </div>
                    </nav>
                </div>
                <hr className='h-px' aria-hidden='true'/>
            </div>
        </div>
    )
}
