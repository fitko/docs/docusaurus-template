module.exports = {
  defaultSidebar: [
    {
      type: 'category',
      label: 'Überblick',
      link: {
        type: 'doc',
        id: 'index',
      },
      collapsible: false,
      items: [
        'SDK',
        'spec',
      ]
    },
    'mermaid',
    'glossary'
  ]
}
